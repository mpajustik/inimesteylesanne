﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inimene
{

    //järgmine samm on õpetajate all printimine korda saada. Praegu ta ei 
    class Program
    {
        static void Main(string[] args)
        {
            Inimene peeter = new Inimene( "Peeter", 38505260021);
            Console.WriteLine(peeter);
            Inimene raul = new Inimene("Raul", 38012314324);
            Console.WriteLine(raul);
            Õpilane anne = new Õpilane("Anne", 48810053445, 4);
            Console.WriteLine(anne);
            Õpetaja elle = new Õpetaja("Elle", 46701120987, "Matemaatika",false, false,false);
            Console.WriteLine(elle);
            Lapsevanem rain = new Lapsevanem("Rain", 38103076512, true, peeter);
            Console.WriteLine(rain);
            Õppeaine matemaatika = new Õppeaine("Matemaatika", 20, elle);
            Console.WriteLine(matemaatika);
            Õpetaja mati = new Õpetaja("Mati", 35512216512, "füüsika", true,true, true, 12, peeter);
            Console.WriteLine(mati);

        }
    }

   public class Inimene
    {
        long _Isikukood;
        string _Nimi;


        public Inimene(string nimi, long isikukood)
        {
            Isikukood = isikukood;
            Nimi = nimi;

        }

        public long Isikukood
        {
            get => _Isikukood;
            set => _Isikukood = value;
        }
        public string Nimi
        {
            get => _Nimi;
            set => _Nimi = value;
        }

        public override string ToString()
        {
            return $"Inimene {Nimi}, isikukoodi number on {Isikukood}";
        }
    }

    class Õpilane : Inimene
    {
        int _Klass;

        public  Õpilane(string nimi, long isikukood, int klass) : base(nimi, isikukood)
        {

            Klass = klass;
            Nimi = nimi;
            Isikukood = isikukood;
        }

        public int Klass
        {
            get => _Klass;
            set => _Klass = value; }

        public override string ToString()
        {
            return $"Inimene {Nimi} isikukoodiga {Isikukood} õpib {Klass} klassis";
        }
    }


    class Lapsevanem : Inimene
    {
        bool _KasOnLapsed;
        object _LapseNimi;

        public Lapsevanem(string nimi, long isikukood, bool kasOnLapsed) : base(nimi, isikukood)
        {
            Nimi = nimi;
            Isikukood = isikukood;
            KasOnLapsed = kasOnLapsed;
        }


        public Lapsevanem(string nimi, long isikukood, bool kasOnLapsed, object lapseNimi) : base(nimi, isikukood)
        {
            Nimi = nimi;
            Isikukood = isikukood;
            KasOnLapsed = kasOnLapsed;
            LapseNimi = lapseNimi;
        }


        public bool KasOnLapsed { get => _KasOnLapsed; set => _KasOnLapsed = value; }
        public object LapseNimi { get => _LapseNimi; set => _LapseNimi = value; }

        public override string ToString()
        {
            if (KasOnLapsed)
            {
                return $"Lapsevanem {Nimi}, tema isikukood on {Isikukood}, tal on laps {KasOnLapsed}, lapse nimi on {LapseNimi}";
            }
            else
            {
                return $"Lapsevanem {Nimi}, tema isikukood on {Isikukood}, tal ei ole last {KasOnLapsed}";
            }


        }
    }

    class Õpetaja : Lapsevanem
    {
        string _Õppeaine;
        bool _Klassijuhataja;
        bool _Õppealajuhataja;
        int _KlassiKlassijuhataja;


        public Õpetaja(string nimi, long isikukood, string õppeaine, bool klassijuhataja, bool õppealajuhataja, bool kasOnLapsed) : base(nimi, isikukood, kasOnLapsed)
        {

            Nimi = nimi;
            Isikukood = isikukood;
            Õppeaine = õppeaine;
            Klassijuhataja = klassijuhataja;
            Õppealajuhataja = õppealajuhataja;
            KasOnLapsed = kasOnLapsed;

        }


        public Õpetaja(string nimi, long isikukood, string õppeaine, bool klassijuhataja, bool õppealajuhataja, bool kasOnLapsed, object lapseNimi): base(nimi, isikukood, kasOnLapsed, lapseNimi)
        {

            Nimi = nimi;
            Isikukood = isikukood;
            Õppeaine = õppeaine;
            Klassijuhataja = klassijuhataja;
            Õppealajuhataja = õppealajuhataja;
            KasOnLapsed = kasOnLapsed;
            LapseNimi = LapseNimi;

        }

        public Õpetaja(string nimi, long isikukood, string õppeaine, bool klassijuhataja, bool õppealajuhataja, bool kasOnLapsed, int klassiKlassijuhataja, object lapseNimi ) : base(nimi, isikukood, kasOnLapsed, lapseNimi)
        {

            Nimi = nimi;
            Isikukood = isikukood;
            Õppeaine = õppeaine;
            Klassijuhataja = klassijuhataja;
            Õppealajuhataja = õppealajuhataja;
            KlassiKlassijuhataja = klassiKlassijuhataja;
            KasOnLapsed = kasOnLapsed;
            LapseNimi = lapseNimi;

        }

        public string Õppeaine { get => _Õppeaine; set => _Õppeaine = value; }
        public bool Klassijuhataja { get => _Klassijuhataja; set => _Klassijuhataja = value; }
        public bool Õppealajuhataja { get => _Õppealajuhataja; set => _Õppealajuhataja = value; }
        public int KlassiKlassijuhataja { get => _KlassiKlassijuhataja; set => _KlassiKlassijuhataja = value; }

        public override string ToString()
        {
            if (Klassijuhataja && Õppealajuhataja && KasOnLapsed)
            {
                return $"Õpetaja {Nimi}, tema isikukood on {Isikukood}, ta õpetab ainet {Õppeaine}, ta on {KlassiKlassijuhataja} klassijuhataja {Klassijuhataja} ja ta on õppealajuhataja {Õppealajuhataja} ja ta on lapsevanem {KasOnLapsed}";
            }
            else if (Klassijuhataja && Õppealajuhataja)
            {
                return $"Õpetaja {Nimi}, tema isikukood on {Isikukood}, ta õpetab ainet {Õppeaine}, ta on {KlassiKlassijuhataja} klassijuhataja {Klassijuhataja} ja ta on õppealajuhataja {Õppealajuhataja}";
            }
            else if (Klassijuhataja && KasOnLapsed)
            {
                return $"Õpetaja {Nimi}, tema isikukood on {Isikukood}, ta õpetab ainet {Õppeaine}, ta on {KlassiKlassijuhataja} klassijuhataja {Klassijuhataja} ja ta on lapsevanem {KasOnLapsed}";
            }
            else if (Õppealajuhataja && KasOnLapsed)
            {
                return $"Õpetaja {Nimi}, tema isikukood on {Isikukood}, ta õpetab ainet {Õppeaine}, ta on õppealajuhataja {Õppealajuhataja} ja ta on lapsevanem {KasOnLapsed}";
            }
            else if (Õppealajuhataja)
            {
                return $"Õpetaja {Nimi}, tema isikukood on {Isikukood}, ta õpetab ainet {Õppeaine} ja ta on õppealajuhataja {Õppealajuhataja}";
            }
            else
            {
                return $"Õpetaja {Nimi}, tema isikukood on {Isikukood}, ta õpetab ainet {Õppeaine}";
            }
           
        }
    }

    

    class Õppeaine
    {
        string _Nimetus;
        int _TundideArv;
        object _ÕpetajaNimi;

        public Õppeaine(string nimetus, int tundideArv, object õpetajaNimi)
        {
            Nimetus = nimetus;
            TundideArv = tundideArv;
            ÕpetajaNimi = õpetajaNimi;
        }

        public string Nimetus { get => _Nimetus; set => _Nimetus = value; }
        public int TundideArv { get => _TundideArv; set => _TundideArv = value; }
        public object ÕpetajaNimi { get => _ÕpetajaNimi; set => _ÕpetajaNimi = value; }

        public override string ToString()
        {
            return $"Õppeaine {Nimetus} kestab {TundideArv} tundi ja tundi viib läbi {ÕpetajaNimi}";
        }
    }
}
